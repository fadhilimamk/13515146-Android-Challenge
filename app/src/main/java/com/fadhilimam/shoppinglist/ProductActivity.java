package com.fadhilimam.shoppinglist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductActivity extends AppCompatActivity {

    public static final String EXTRA_PRODUCT_NAME = "PRODUCT_NAME";
    public static final String EXTRA_PRODUCT_IMAGE = "PRODUCT_IMAGE";

    @BindView(R.id.product_list) protected RecyclerView mProductList;

    private final ArrayList<Product> productData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_product);
        ButterKnife.bind(this);

        initializeProduct();
        initializeList();

    }

    private void initializeProduct() {
        productData.add(new Product(R.drawable.item_banana, "Banana"));
        productData.add(new Product(R.drawable.item_apple, "Apple"));
        productData.add(new Product(R.drawable.item_cherries, "Cherries"));
        productData.add(new Product(R.drawable.item_durian, "Durian"));
        productData.add(new Product(R.drawable.item_grapes, "Grapes"));
        productData.add(new Product(R.drawable.item_pineapple, "Pineapple"));
        productData.add(new Product(R.drawable.item_starberry, "Strawberry"));
        productData.add(new Product(R.drawable.item_watermelon, "Watermelon"));
    }

    private void initializeList() {

        ProductAdapter.buttonClickListener listener = new ProductAdapter.buttonClickListener() {
            @Override
            public void onButtonClickListener(Product data) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(EXTRA_PRODUCT_NAME, data.getName());
                resultIntent.putExtra(EXTRA_PRODUCT_IMAGE, data.getImageRes());
                setResult(RESULT_OK,resultIntent);
                finish();
            }
        };

        ProductAdapter adapter = new ProductAdapter(productData, listener, true);
        mProductList.setLayoutManager(new LinearLayoutManager(this));
        mProductList.setAdapter(adapter);
    }


}
